import React from 'react';
import './App.css';
import { subscribeToTimer } from './api';


class App extends React.Component {

  state = {
    timestamp: 'no timestamp yet'
  };

  componentDidMount() {
    subscribeToTimer((err, timestamp) => console.log(timestamp));
  }




  render() {
    return(
        <div className="App">
          <p className="App-intro">
            This is the timer value: {this.state.timestamp}
          </p>
        </div>
    )
  }
}

export default App;
